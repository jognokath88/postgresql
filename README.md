# Working Output - PostgreSQL + KNEX with jsonb and pgcrypto


[![](./media/pg-crypto.png)](https://youtu.be/LmsUMDZ4EWY "Working Output - PostgreSQL + KNEX with jsonb and pgcrypto by kate")

<figure class="video_container">
  <iframe src="./media/pg-crypto.mkv" frameborder="0" allowfullscreen="true"> 
</iframe>
</figure>

# Working Output - PostgreSQL and KNEX


[![](./media/postgres-knex.png)](https://youtu.be/Vf1s8KILlqA "Working Output - PostgreSQL and Knex by kate")

<figure class="video_container">
  <iframe src="./media/postgres-knex.mkv" frameborder="0" allowfullscreen="true"> 
</iframe>
</figure>
