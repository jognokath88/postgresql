import knex from './postgres.js';
import fs from 'fs';
const pubKey = fs.readFileSync('./public.key', 'utf-8').toString();
const secretKey = fs.readFileSync('./secret.key','utf-8').toString();


function addCard(card) {
    return knex('cardz')
        .insert({
            name: knex.raw(`pgp_pub_encrypt('${card.name}', dearmor('${pubKey}'))`),
            details : card.details
        }).returning("*");
}

function getAllCards() {
    return knex('cardz')
            .select(
                'id', 
                knex.raw(`pgp_pub_decrypt(name::bytea, dearmor('${secretKey}'),'kwekkwek88') as name`),
                'details'
            ).returning('*');
}
   
function getSingleCard(id) {

    return knex('cardz')
            .select(
                'id', 
                knex.raw(`pgp_pub_decrypt(name::bytea, dearmor('${secretKey}'),'kwekkwek88') as name`),
                'details')
            .where({ id: parseInt(id) });
}

function updateCard(id, card) {
    return knex('cardz')
        .update({
                name : knex.raw(`pgp_pub_encrypt('${card.name}', dearmor('${pubKey}'))`),
                details : card.details
            })
        .where({ id: parseInt(id) })
        .returning('*');
}

function deleteCard(id) {
    return knex('cardz')
    .del()
    .where({ id: parseInt(id) })
    .returning("*");
}

export default {
    getAllCards, addCard, getSingleCard, updateCard, deleteCard
};