// reference: https://mherman.org/blog/building-a-restful-api-with-koa-and-postgres/

import config from './knexfile.js';
import knex from 'knex';

export default knex(config['development']);