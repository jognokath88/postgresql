
import Koa from 'koa';
import Router from '@koa/router';
import jsonBody from 'koa-json-body';
import koaLogger from 'koa-logger';
import queries from './model.js';

const BASE_URL = `/api/v1/cards`;

// import { postgresMiddleware, postgres } from './postgres.js';
// import {schema, insert, update, retrieve, retrieveALL} from './model.js';

const app = new Koa()
    .use(koaLogger())
    .use(jsonBody());
    // .use(postgresMiddleware(schema));

const router = new Router()
    .get(BASE_URL, async (ctx) => {
        try {
        const cards = await queries.getAllCards();
        ctx.body = {
            status: 'All cards retrieved successfully',
            data: cards
        };
        } catch (err) {
        console.log(err)
        }
    })

    .get(`${BASE_URL}/:id`, async (ctx) => {
        try {
          const card = await queries.getSingleCard(ctx.params.id);
          if (card.length) {
            ctx.status = 200;
            ctx.body = {
              status: 'Retrieved successfully',
              data: card
            };
          } else {
            ctx.status = 404;
            ctx.body = {
              status: 'error retrieving card',
              message: 'That card does not exist.'
            };
          }
          
        } catch (err) {
          console.log(err)
        }
      })
    
    .post(`${BASE_URL}`, async (ctx) => {
        try {
          const card = await queries.addCard(ctx.request.body);
          if (card.length) {
            ctx.status = 201;
            ctx.body = {
              status: 'Added card successfully',
              data: card
            };
          } else {
            ctx.status = 400;
            ctx.body = {
              status: 'error',
              message: 'Something went wrong.'
            };
          }
        } catch (err) {
          console.log(err)
        }

    })
    
    .put(`${BASE_URL}/:id`, async (ctx) => {
        try {
          const card = await queries.updateCard(ctx.params.id, ctx.request.body);
          if (card.length) {
            ctx.status = 200;
            ctx.body = {
              status: 'Updated successfully',
              data: card
            };
          } else {
            ctx.status = 404;
            ctx.body = {
              status: 'error',
              message: 'That card does not exist.'
            };
          }
        } catch (err) {
          ctx.status = 400;
          ctx.body = {
            status: 'error',
            message: err.message || 'Sorry, an error has occurred.'
          };
        }
    })
    
    .delete(`${BASE_URL}/:id`, async (ctx) => {
        try {
          const card = await queries.deleteCard(ctx.params.id);
          if (card.length) {
            ctx.status = 200;
            ctx.body = {
              status: 'Deleted successfully',
              data: card
            };
          } else {
            ctx.status = 404;
            ctx.body = {
              status: 'error',
              message: 'That card does not exist.'
            };
          }
        } catch (err) {
          ctx.status = 400;
          ctx.body = {
            status: 'error',
            message: err.message || 'Sorry, an error has occurred.'
          };
        }
    });

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(8080, () => console.log('Listening on port 8080'));