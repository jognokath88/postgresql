import fs from 'fs';
// create pgp public and private keys
const pubKey = fs.readFileSync('./public.key', 'utf-8').toString();
const secretKey = fs.readFileSync('./secret.key','utf-8').toString();

export const seed = (knex, Promise) => {
    return knex('cardz').del()
    .then(() => {
      return knex('cardz').insert(
        
        {
        name: knex.raw(`pgp_pub_encrypt('charmander', dearmor('${pubKey}'))`),
        details : {
            hit_points: 70,
            type: 'fire',
            info : 'No. 004 Lizard Pokemon HT: 2 ft. 00 in. WT: 18.7 lbs.',
            moves : [
                {
                    name: 'scratch', 
                    damage: 10
                },
                {
                    name: 'ember', 
                    damage: 30
                }
            ],
            weakness : 'water'
        }
      });
    })
    .then(() => {
      return knex('cardz').insert({
        name: knex.raw(`pgp_pub_encrypt('squirtle', dearmor('${pubKey}'))`),
        details : {
            hit_points: 50,
            type: 'water',
            info : 'No. 007 Tiny Squirtle Pokemon HT: 1 ft. 08 in. WT: 19.8 lbs.',
            moves : [
                {
                    name: 'bubble', 
                    damage: 0
                },
                {
                    name: 'aqua tail', 
                    damage: 20
                }
            ],
            weakness : 'electric'
        }
      });
    })
    .then(() => {
      return knex('cardz').insert({
        name: knex.raw(`pgp_pub_encrypt('blastoise', dearmor('${pubKey}'))`),
        details : {
            hit_points: 100,
            type: 'water',
            info : 'Shellfish Pokemon HT: 5 ft. 3 in. WT: 189 lbs.',
            moves : [
                {
                    name: 'bubble', 
                    damage: 20
                },
                {
                    name: 'double cannon', 
                    damage: 40
                }
            ],
            weakness : 'electric'
        }
      });
    });
  };
