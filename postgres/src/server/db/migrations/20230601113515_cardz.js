export const up = (knex, Promise) => {
    return knex.schema.raw('CREATE EXTENSION IF NOT EXISTS "pgcrypto"')
                .createTable('cardz', (table) => {
                table.bigIncrements('id').primary(); // big serial
                table.text('name').notNullable();
                table.jsonb('details').nullable();
                });
  };
  
export const down = (knex, Promise) => {
    return knex.schema.dropTable('cardz');
};
